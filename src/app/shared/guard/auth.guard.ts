import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {
    readonly MINUTES_UNITL_AUTO_LOGOUT = 10; // in mins
    readonly CHECK_INTERVAL = 1000; // in ms
    readonly STORE_KEY = 'lastAction';

    constructor(private router: Router) {
        this.check();
        this.initListener();
        this.initInterval();
     }

    canActivate() {
        if (localStorage.getItem('isLoggedin')) {
            return true;
        }

        this.router.navigate(['/login']);
        return false;
    }

    get lastAction() {
        return parseInt(localStorage.getItem(this.STORE_KEY));
    }

    set lastAction(value) {
        localStorage.setItem(this.STORE_KEY, value + '');
    }

    initListener() {
        document.body.addEventListener('click', () => this.reset());
    }

    reset() {
        this.lastAction = Date.now();
    }

    initInterval() {
        setInterval(() => {
            this.check();
      }, this.CHECK_INTERVAL);
    }

    check() {
      const now = Date.now();
      const timeleft = this.lastAction + this.MINUTES_UNITL_AUTO_LOGOUT * 60 * 1000;
      const diff = timeleft - now;
      const isTimeout = diff < 0;

      if (isTimeout && localStorage.getItem('isLoggedin')) {
        this.router.navigate(['/login']);
      }
    }
}
