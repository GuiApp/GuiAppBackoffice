import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../environments/environment';

import 'rxjs/add/operator/toPromise';

import { Configuration } from './api/configuration';

export class UploadFileResponse {
    public status: string;
    public originalName: string;
    public generatedName: string;
}

@Injectable()
export class UploadFileService {
    readonly DEBUG = true;
    public stockImgUrl = environment.imageStock; // 'https://guiappworldwide.com/imgstock/stock/';
    public stockImgApi = environment.stockImgApi; // 'https://guiappworldwide/imgstock/index.php';
    // public stockImgUrl = 'http://192.168.1.24/appquimia/imgstock/';
    // public stockImgApi = 'http://192.168.1.24/appquimia/index.php';
    private headers;

    constructor(private http: Http, private configuration: Configuration) {
        this.headers = new Headers();
        this.headers.append('Accept', 'application/json');
    }

    public upload(file: File): Promise<UploadFileResponse> {
        const formData: FormData = new FormData();
        formData.append('file', file, file.name);
        const headers = new Headers();
        const options = new RequestOptions({ headers: this.headers });
        return this.http
            .post(`${this.stockImgApi}`, formData, options)
            .toPromise()
            .then(response => {
                const entity = response.json() as UploadFileResponse;
                entity.generatedName = `${this.stockImgUrl}${entity.generatedName}`;
                return entity;
                })
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    extractData(res: Response) {
        return res.text() ? res.json() : {}; ;
    }
}
