import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

// import { Configuration } from './configuration';

export class Empresa {
  public  id: number;
  public  razonsocial: string;
  public  cuit: number;
  public  mediodepago: string;
  public  estado: string;
  public  logo: string;
  public  facebook: string;
  public  twitter: string;
  public  instagram: string;
  public  palabrasclave: string;
  public  idimagen: number;
  public  dirty: number;
  public  web: string;
  public  descripcion: string;
  public  idcuenta: number;
}

@Injectable()
export class EmpresaService {
  readonly GET_LIST = 'l';
  readonly GET_BYID = 'byid';

  private apiUrl = '';
  private headers;
  constructor (private http: Http) {
    this.apiUrl = environment.apiUrl + 'empresa';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
  }

  getList(): Promise<Empresa[]> {
    const customUrl = `${this.apiUrl}/${this.GET_LIST}`;

    return this.http
      .get(customUrl)
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Empresa[];
        return entities;
        }
      ).catch(e => this.handleError(e));
  }

  getByID(id: string): Promise<Empresa> {
    const customUrl = `${this.apiUrl}/${this.GET_BYID}/${id}`;
    return this.http
      .get(customUrl)
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as Empresa;
          return entity;
        }
      ).catch(e => this.handleError(e));
}

  save(entity: Empresa): Promise<Empresa> {
    if (entity.id) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: Empresa): Promise<Empresa> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity))
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: Empresa): Promise<Empresa> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity))
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      ).catch(e => this.handleError(e));
  }

  delete(entity: Empresa): Promise<Response> {
      const customUrl = `${this.apiUrl}/${entity.id}`;

      return this.http
          .delete(customUrl)
          .toPromise()
          .then(response =>  {
              return response.ok;
          })
          .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
