import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

export class Usuario {
    public id: number;
    public usuario: string;
    public contrasena: string;
}

@Injectable()
export class UsuarioService {
    private apiUrl = '';
    private headers;
    private usuarios: Usuario[];

    constructor(private http: Http) {
        this.apiUrl = environment.apiUrl + 'usuario';
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
    }

    getUsuarios(): Promise<Usuario[]> {

        const options = new RequestOptions({ headers: this.headers });

        return this.http
            .get(this.apiUrl)
            .toPromise()
            .then(response => {
                const body = response.json();
                const usuarios = body || {} as Usuario[];
                return usuarios;
            }
            )
            .catch(this.handleError);
    }

    autenticate(usuarioName: string, clave: string): Promise<Boolean> {
        const options = new RequestOptions({ headers: this.headers });

        const json_req = JSON.stringify({
            'auth':
            {
                'usuario': usuarioName,
                'contrasena': clave
            }
        });

        return this.http
            .post(this.apiUrl, json_req)
            .toPromise()
            .then(res => {
                const body = res.json();

                const fecha = new Date;
                const startSession = JSON.stringify({
                        'usuario': usuarioName,
                        'initsession': {
                            'year': fecha.getFullYear(),
                            'month': fecha.getMonth(),
                            'day': fecha.getDay(),
                            'hour': fecha.getHours(),
                            'minute': fecha.getMinutes()}});

                localStorage.setItem('userLog', startSession);
                return body.status === 'success';
            })
            .catch(this.handleError);
    }

    getUsuario(id: number): Promise<Usuario> {
        return this.getUsuarios()
            .then(usuarios => usuarios.find(usuario => usuario.id === id));
    }

    getUsuarioByNombre(nombre: string): Promise<Usuario> {
        return this.getUsuarios()
            .then(usuarios => usuarios.find(usuario => usuario.usuario === nombre));
    }

    save(usuario: Usuario): Promise<Usuario> {
        if (usuario.id) {
            return this.put(usuario);
        }
        return this.post(usuario);
    }

    delete(usuario: Usuario): Promise<Response> {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');

        const url = `${this.apiUrl}/${usuario.id}`;

        return this.http
            .delete(url, { headers: headers })
            .toPromise()
            .catch(this.handleError);
    }

    private post(usuario: Usuario): Promise<Usuario> {
        const headers = new Headers({
            'Content-Type': 'application/json'
        });

        return this.http
            .post(this.apiUrl, JSON.stringify(usuario), { headers: headers })
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    private put(usuario: Usuario): Promise<Usuario> {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');

        const url = `${this.apiUrl}/${usuario.id}`;

        return this.http
            .put(url, JSON.stringify(usuario), { headers: headers })
            .toPromise()
            .then(() => usuario)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
