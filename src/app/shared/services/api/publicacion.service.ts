import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

// import { Configuration } from './configuration';

export class Publicacion {
  public  id: number;
  public  titulo: string;
  public  imagen: number;
  public  descripcion: string;
  public  idempresa: number;
  public  razonsocial: string;
  public  dirty: number;
  public  destacada: number;
  public  fecmodificacion: string;
}

@Injectable()
export class PublicacionService {
  readonly GET_LISTBACKOFFICE = 'listabackoffice';
  readonly GET_BYID = 'byid';

  private apiUrl = '';
  private headers;
  constructor (private http: Http) {
    this.apiUrl = environment.apiUrl + 'publicacion';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
  }

  getList(): Promise<Publicacion[]> {
    const customUrl = `${this.apiUrl}/${this.GET_LISTBACKOFFICE}`;

    return this.http
      .get(customUrl)
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Publicacion[];
        return entities;
        }
      ).catch(e => this.handleError(e));
  }

  getByID(id: string): Promise<Publicacion> {
    const customUrl = `${this.apiUrl}/${this.GET_BYID}/${id}`;
    return this.http
      .get(customUrl)
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as Publicacion;
          return entity;
        }
      ).catch(e => this.handleError(e));
  }

  save(entity: Publicacion): Promise<Publicacion> {
    if (entity.id) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: Publicacion): Promise<Publicacion> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity))
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: Publicacion): Promise<Publicacion> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity))
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      ).catch(e => this.handleError(e));
  }

  delete(entity: Publicacion): Promise<Response> {
      const customUrl = `${this.apiUrl}/${entity.id}`;

      return this.http
          .delete(customUrl)
          .toPromise()
          .then(response =>  {
              return response.ok;
          })
          .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
