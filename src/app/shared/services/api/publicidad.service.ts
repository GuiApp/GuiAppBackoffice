import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

// import { Configuration } from './configuration';

export class Publicidad {
    public id: number;
    public idsponsor: number;
    public razonsocial: string;
    public fecinicio: string;
    public fecfin: string;
    public imagen: string;
    public url: string;

    constructor() {
        this.imagen = 'https://placehold.it/380x130';
    }
}

@Injectable()
export class PublicidadService {

    private apiUrl = '';
    private headers;
    constructor(private http: Http) {
        this.apiUrl = environment.apiUrl + 'publicidad';
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
    }

    getList(): Promise<Publicidad[]> {
        const customUrl = `${this.apiUrl}`;

        return this.http
            .get(customUrl)
            .toPromise()
            .then(response => {
                const body = response.json();
                const entities = body || {} as Publicidad[];
                return entities;
            }
            ).catch(e => this.handleError(e));
    }

    getByID(id: number): Promise<Publicidad> {
        return this.http
            .get(this.apiUrl + '/' + id)
            .toPromise()
            .then(response => {
                const body = response.json();
                const entity = body[0] as Publicidad;
                return entity;
            }
            ).catch(e => this.handleError(e));
    }

    save(entity: Publicidad): Promise<Publicidad> {
        if (entity.id) {
            return this.put(entity);
        }
        return this.post(entity);
    }

    private post(entity: Publicidad): Promise<Publicidad> {
        return this.http
            .post(this.apiUrl, JSON.stringify(entity))
            .toPromise()
            .then(response => {
                const body = response.json();
                return body.status === 'success';
            }
            )
            .catch(e => this.handleError(e));
    }

    private put(entity: Publicidad): Promise<Publicidad> {
        const url = `${this.apiUrl}/${entity.id}`;

        return this.http
            .put(url, JSON.stringify(entity))
            .toPromise()
            .then(response => {
                const body = response.json();
                return body.status === 'success';
            }
            ).catch(e => this.handleError(e));
    }

    delete(entity: Publicidad): Promise<Response> {
        const customUrl = `${this.apiUrl}/${entity.id}`;

        return this.http
            .delete(customUrl)
            .toPromise()
            .then(response => {
                if (response.status === 204) {
                    return response.ok;
                } else if (response.status === 202) {
                    return -1;
                }
            })
            .catch(
                error =>
                    this.handleError(error)
            );
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
