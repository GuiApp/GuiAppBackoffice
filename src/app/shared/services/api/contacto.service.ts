import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

// import { Configuration } from './configuration';

export class Contacto {
  public id: number;
  public apellidos: string;
  public nombres: string;
  public email: string;
  public telefono: string;
  public cargo: string;
  public idsponsor: number;

  constructor() {
    this.apellidos = '';
  }
}

@Injectable()
export class ContactoService {
  readonly GET_LISTBYSPONSOR = 's';
  private apiUrl = '';
  private headers;
  constructor (private http: Http) {
    this.apiUrl = environment.apiUrl + 'contacto';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
  }

  getList(): Promise<Contacto[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl)
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Contacto[];
        return entities;
        }
      ).catch(e => this.handleError(e));
  }

  getListBySponsor(idsponsor: number): Promise<Contacto[]> {
    const customUrl = `${this.apiUrl}/${this.GET_LISTBYSPONSOR}${idsponsor}`;

    return this.http
      .get(customUrl)
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Contacto[];
        return entities;
        }
      ).catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<Contacto> {
    return this.http
      .get(this.apiUrl + '/' + id)
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as Contacto;
          return entity;
        }
      ).catch(e => this.handleError(e));
  }

  save(entity: Contacto): Promise<Contacto> {
    if (entity.id) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: Contacto): Promise<Contacto> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: Contacto): Promise<Contacto> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity))
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      ).catch(e => this.handleError(e));
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
