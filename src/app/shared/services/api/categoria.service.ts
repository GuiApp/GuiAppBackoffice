import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

// import { Configuration } from './configuration';

export class Categoria {
  public  id: number;
  public  descripcion: string;
}

@Injectable()
export class CategoriaService {

  private apiUrl = '';
  private headers;
  constructor (private http: Http) {
    this.apiUrl = environment.apiUrl + 'categoria';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
  }

  getList(): Promise<Categoria[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl)
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Categoria[];
        return entities;
        }
      ).catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<Categoria> {
    return this.http
      .get(this.apiUrl + '/' + id)
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as Categoria;
          return entity;
        }
      ).catch(e => this.handleError(e));
  }

  save(entity: Categoria): Promise<Categoria> {
    if (entity.id) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: Categoria): Promise<Categoria> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity))
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: Categoria): Promise<Categoria> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity))
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      ).catch(e => this.handleError(e));
  }

  delete(entity: Categoria): Promise<Response> {
      const customUrl = `${this.apiUrl}/${entity.id}`;

      return this.http
          .delete(customUrl)
          .toPromise()
          .then(response =>  {
              return response.ok;
          })
          .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
