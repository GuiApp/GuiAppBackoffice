import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

// import { Configuration } from './configuration';

export class Evento {
  public id: string;
  public nombre: string;
  public descripcion: string;
  public ubicacion: string;
  public fecinicio: string;
  public fecfin: string;
  public idempresa: number;
  public razonsocial: string;
  public imagen: string;
  public dirty: number;

  constructor() {
    this.fecfin = '';
  }
}

@Injectable()
export class EventoService {
  readonly GET_LISTA = 'lista';
  readonly GET_LISTABACKOFFICE = 'listabackoffice';
  private apiUrl = '';
  private headers;
  constructor (private http: Http) {
    this.apiUrl = environment.apiUrl + 'evento';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
  }

  getListAll(): Promise<Evento[]> {
    const customUrl = `${this.apiUrl}/${this.GET_LISTABACKOFFICE}`;

    return this.http
      .get(customUrl)
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Evento[];
        return entities;
        }
      ).catch(e => this.handleError(e));
  }

  getListByReclamo(idreclamo: string): Promise<Evento[]> {
    const customUrl = `${this.apiUrl}/${this.GET_LISTABACKOFFICE}${idreclamo}`;

    return this.http
      .get(customUrl)
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Evento[];
        return entities;
        }
      ).catch(e => this.handleError(e));
  }

  getByID(id: string): Promise<Evento> {
    return this.http
      .get(this.apiUrl + '/' + id)
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as Evento;
          return entity;
        }
      ).catch(e => this.handleError(e));
  }

  save(entity: Evento): Promise<Evento> {
    if (entity.id) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: Evento): Promise<Evento> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity))
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: Evento): Promise<Evento> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity))
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      ).catch(e => this.handleError(e));
  }

  delete(entity: Evento): Promise<Response> {
      const customUrl = `${this.apiUrl}/${entity.id}`;

      return this.http
          .delete(customUrl)
          .toPromise()
          .then(response =>  {
              return response.ok;
          })
          .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
