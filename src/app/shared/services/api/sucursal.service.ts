import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

// import { Configuration } from './configuration';

export class Sucursal {
  public  id: string;
  public  idempresa: string;
  public  razonsocial: string;
  public  direccion: string;
  public  telefono: string;
  public  delivery: number;
  public  veinticuatrohs: number;
  public  diashorarios: string;
  public  dirty: number;
  public  palabrasclave: string;
  public  latitud: string;
  public  longitud: string;
}

@Injectable()
export class SucursalService {
  readonly GET_POR_EMPRESA = 'porempresa';
  readonly GET_BYID = 'byid';

  private apiUrl = '';
  private headers;
  constructor (private http: Http) {
    this.apiUrl = environment.apiUrl + 'sucursal';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
  }

  getList(): Promise<Sucursal[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl)
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Sucursal[];
        return entities;
        }
      ).catch(e => this.handleError(e));
  }

  getListByIdEmpresa(idempresa: string): Promise<Sucursal[]> {
    const customUrl = `${this.apiUrl}/${this.GET_POR_EMPRESA}/${idempresa}`;

    return this.http
      .get(customUrl)
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Sucursal[];
        return entities;
        }
      ).catch(e => this.handleError(e));
  }

  getByID(id: string): Promise<Sucursal> {
    const customUrl = `${this.apiUrl}/${this.GET_BYID}/${id}`;
    return this.http
      .get(customUrl)
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as Sucursal;
          return entity;
        }
      ).catch(e => this.handleError(e));
  }

  save(entity: Sucursal): Promise<Sucursal> {
    if (entity.id) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: Sucursal): Promise<Sucursal> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity))
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: Sucursal): Promise<Sucursal> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity))
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      ).catch(e => this.handleError(e));
  }

  delete(entity: Sucursal): Promise<Response> {
      const customUrl = `${this.apiUrl}/${entity.id}`;

      return this.http
          .delete(customUrl)
          .toPromise()
          .then(response =>  {
              return response.ok;
          })
          .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
