import { Injectable } from '@angular/core';

@Injectable()
export class Configuration {
    // public Server = 'http://192.168.1.24/';
    // public ApiUrl = 'api-guiapp/';
    // public ImgStock = 'guiapp/imgstock/index.php'
    public Server = 'http://guiappworldwide.com/admin/';
    public ApiUrl = 'api/';
    public ImgStock = 'admin/imgstock/index.php'
    public ServerWithApiUrl = this.Server + this.ApiUrl;
    public ServerWithImgStockUrl = this.Server + this.ImgStock;
    public SessionExpire = 900000
}
