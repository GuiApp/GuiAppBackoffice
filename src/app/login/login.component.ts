import { Component, Input, Output, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';

import { Usuario, UsuarioService } from '../shared/services';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()],
    providers: [UsuarioService]
})
export class LoginComponent implements OnInit {
    public error = false;
    loading = false;

    @Input() user;
    @Input() pass;

    constructor(public router: Router, private usuarioService: UsuarioService) {
    }

    ngOnInit() {
    }

    onLoggedin(user, pass) {
        this.loading = true;
        this.usuarioService.autenticate(user, pass).then(
            response => {
                if (response) {
                    localStorage.setItem('isLoggedin', 'true');
                    this.error = false;
                    this.router.navigate(['/dashboard']);
                } else {
                    this.error = true;
                    this.loading = false;
                }
            }
        ).catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
