import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { DatePipe } from '@angular/common';

import { Publicacion, PublicacionService } from '../../shared';

@Component({
    selector: 'app-tables',
    templateUrl: './publicaciones.component.html',
    styleUrls: ['./publicaciones.component.scss'],
    animations: [routerTransition()],
    providers: [
        DatePipe,
        PublicacionService ]
})

export class PublicacionesComponent implements OnInit {
settings = {
    noDataMessage: 'Datos no disponibles',
    pager: { display: true, perPage: 10 },
    columns: {
        // id: { title: 'Código', filter: false, },
        razonsocial: { title: 'Razón Social', filter: false, },
        titulo: { title: 'Título', filter: false, },
        descripcion: { title: 'Descripción', filter: false, },
        destacada: {
            title: 'Destacada', filter: false,
            type: 'html',
            valuePrepareFunction: (value) => {
                return (+value === 1) ? 'SI' : 'NO';
            }
        },
        fecmodificacion: {
            title: 'Fecha  de Modificación', filter: false,
            type: 'html',
            valuePrepareFunction: (value) => {
                return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy') + '</div>';
            }
        },
    },
    defaultStyle: false,
    rowClassFunction : function(row) {
            const aprobado = +row.data.dirty;
            if (aprobado === 1) {
                return 'aprobado';
            } if (aprobado === 0) {
                return 'noaprobado';
            } else {
                return '';
            }
    },
    actions: { add: false, edit: false, delete: false, },
    attr: {
        class: 'table dataTable table-striped table-bordered',
    }
};
source: LocalDataSource;

    public dataEstados: Array<any> = Array<any>();
    public selectedEntity: Publicacion;
    public error = false;
    public loading = false;
    public data: Array<any> = Array<any>();

    public constructor(
        public router: Router,
        private datePipe: DatePipe,
        private publicacionService: PublicacionService) {
            this.source = new LocalDataSource(this.data);
    }

    public ngOnInit(): void {
        this.publicacionService.getList().then(
            response => {
                if (response) {
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                } else {
                    alert('No se pudo obtener los datos de Empresas!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'titulo', search: query },
            { field: 'descripcion', search: query },
            { field: 'razonsocial', search: query },
            { field: 'destacada', search: query },
            { field: 'fecmodificacion', search: query }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data;
        this.router.navigate(['/publicacion', event.data.id]);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
