import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { PageHeaderModule } from './../../shared';
import { Ng2SmartTableModule } from 'ng2-smart-table';


import { PublicacionesRoutingModule } from './publicaciones-routing.module';
import { PublicacionesComponent } from './publicaciones.component';


@NgModule({
    imports: [
        FormsModule,
        Ng2SmartTableModule,
        CommonModule,
        PublicacionesRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule
    ],
    declarations: [PublicacionesComponent]
})
export class PublicacionesModule { }
