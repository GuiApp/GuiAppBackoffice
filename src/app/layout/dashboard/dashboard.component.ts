import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';

// import{
//     Resumen, ReclamoService
// } from '../../shared';

@Component({ 
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()],
    // providers: [ReclamoService]
})
export class DashboardComponent implements OnInit {
    public alerts: Array<any> = [];
    public sliders: Array<any> = [];

    // public resumen: Resumen;

    constructor(
        // private reclamoService: ReclamoService
    ) {
            // this.resumen = new Resumen();
        this.sliders.push({
            imagePath: 'assets/images/slider1.jpg',
            label: 'Encontra Eventos',
            text: 'Todos los eventos en tu ciudad.'
        }, {
            imagePath: 'assets/images/slider2.jpg',
            label: 'Servicios y Locales',
            text: 'Encontra todos los locales y servicios cerca tuyo.'
        }, {
            imagePath: 'assets/images/slider3.jpg',
            label: 'Se Auspiciante',
            text: 'Consúltanos para convertirte en sponsor.'
        });

/*
        this.alerts.push({
            id: 1,
            type: 'success',
            message: `Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Voluptates est animi quibusdam praesentium quam, et perspiciatis,
                consectetur velit culpa molestias dignissimos
                voluptatum veritatis quod aliquam! Rerum placeat necessitatibus, vitae dolorum`
        }, {
            id: 2,
            type: 'warning',
            message: `Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Voluptates est animi quibusdam praesentium quam, et perspiciatis,
                consectetur velit culpa molestias dignissimos
                voluptatum veritatis quod aliquam! Rerum placeat necessitatibus, vitae dolorum`
        });*/
    }

    ngOnInit() {
        // this.getData();
    }

    // public getData(): any {
    //     this.reclamoService.getResumen().then(
    //         response => {
    //             this.resumen = response as Resumen;
    //         }
    //     ).catch(e => this.handleError(e));
    // }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }

  /*  public reclamosTotal() {
        console.log('reclamosTotal');
    }*/

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
