import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { PageHeaderModule } from './../../shared';
import { Ng2SmartTableModule } from 'ng2-smart-table';


import { CategoriasRoutingModule } from './categorias-routing.module';
import { CategoriasComponent } from './categorias.component';


@NgModule({
    imports: [
        FormsModule,
        Ng2SmartTableModule,
        CommonModule,
        CategoriasRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule
    ],
    declarations: [CategoriasComponent]
})
export class CategoriasModule { }
