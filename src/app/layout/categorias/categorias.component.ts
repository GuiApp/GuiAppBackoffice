import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';

import { Categoria, CategoriaService } from '../../shared';

@Component({
    selector: 'app-tables',
    templateUrl: './categorias.component.html',
    styleUrls: ['./categorias.component.scss'],
    animations: [routerTransition()],
    providers: [ CategoriaService ]
})

export class CategoriasComponent implements OnInit {
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            id: { title: 'Código', filter: false, },
            descripcion: { title: 'Descripción', filter: false, }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;

    public dataEstados: Array<any> = Array<any>();
    public selectedEntity: Categoria;
    public error = false;
    public loading = false;
    public data: Array<any> = Array<any>();

    public constructor(public router: Router, private route: ActivatedRoute,
        private categoriaService: CategoriaService) {
            this.source = new LocalDataSource(this.data);
    }

    public ngOnInit(): void {
        this.categoriaService.getList().then(
            response => {
                if (response) {
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                } else {
                    alert('No se pudo obtener los datos de Empresas!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'id', search: query },
            { field: 'descripcion', search: query }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data;
        this.router.navigate(['/categoria', event.data.id]);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
