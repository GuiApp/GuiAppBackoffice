import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { DatePipe } from '@angular/common';

import { Evento, EventoService } from '../../shared';

@Component({
    selector: 'app-tables',
    templateUrl: './eventos.component.html',
    styleUrls: ['./eventos.component.scss'],
    animations: [routerTransition()],
    providers: [
        DatePipe,
        EventoService ]
})

export class EventosComponent implements OnInit {
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            // id: { title: 'Código', filter: false, },
            nombre: { title: 'Nombre', filter: false, },
            descripcion: { title: 'Descripción', filter: false, },
            ubicacion: { title: 'Ubicación', filter: false, },
            fecinicio: {
                title: 'Fecha Inicio', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy') + '</div>';
                }
            },
            // fecfin: {
            //     title: 'Fecha Fin', filter: false,
            //     type: 'html',
            //     valuePrepareFunction: (value) => {
            //         return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy') + '</div>';
            //     }
            // },
            razonsocial: { title: 'Empresa', filter: false, }
        },
        defaultStyle: false,
        rowClassFunction : function(row) {
                const aprobado = +row.data.dirty;
                if (aprobado === 1) {
                    return 'aprobado';
                } if (aprobado === 0) {
                    return 'noaprobado';
                } else {
                    return '';
                }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;

    public dataEstados: Array<any> = Array<any>();
    public selectedEntity: Evento;
    public data: Array<any> = Array<any>();

    public constructor(
        public router: Router,
        private datePipe: DatePipe,
        private eventoService: EventoService) {
            this.source = new LocalDataSource(this.data);
    }

    public ngOnInit(): void {
        this.eventoService.getListAll().then(
            response => {
                if (response) {
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                } else {
                    alert('No se pudo obtener los datos de Empresas!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'nombre', search: query },
            { field: 'descripcion', search: query },
            { field: 'ubicacion', search: query },
            { field: 'fecinicio', search: query },
            { field: 'fecfin', search: query },
            { field: 'razonsocial', search: query }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data;
        this.router.navigate(['/evento', event.data.id]);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
