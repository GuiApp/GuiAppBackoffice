import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { PageHeaderModule } from './../../shared';
import { Ng2SmartTableModule } from 'ng2-smart-table';


import { EventosRoutingModule } from './eventos-routing.module';
import { EventosComponent } from './eventos.component';

@NgModule({
    imports: [
        FormsModule,
        Ng2SmartTableModule,
        CommonModule,
        EventosRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule
    ],
    declarations: [EventosComponent]
})
export class EventosModule { }
