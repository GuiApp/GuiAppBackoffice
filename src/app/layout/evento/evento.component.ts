import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import {
    Evento, EventoService
} from '../../shared/services';

@Component({
    selector: 'app-form',
    templateUrl: './evento.component.html',
    styleUrls: ['./evento.component.scss'],
    animations: [routerTransition()],
    providers: [
        EventoService]
})
export class EventoComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Evento';

    public error = false;
    public loading = false;
    submitted = false;
    public selectedID: string;
    public selectedEntity: Evento;
    public dataContactos: Array<any> = Array<any>();

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private location: Location,
        private modalService: NgbModal,
        private eventoService: EventoService) {
        this.selectedEntity = new Evento();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.loading = true;
            this.selectedID = params['id'];
            this.getData();
            this.loading = false;
        });
    }

    public getData() {
      if (this.selectedID.length > 0) {
          this.eventoService.getByID(this.selectedID)
          .then(
              response => {
                  this.loading = false;
                  this.selectedEntity = response as Evento;
                              })
          .catch(e => this.handleError(e));
      }
    }


    public guardar(content) {
        this.eventoService
            .save(this.selectedEntity)
            .then(
                response => {
                    if (response) {
                        this.loading = false;
                        this.selectedID = response + '';
                        this.showMessage(content, 'El evento se guardó correctamente', this.MESSAGE_GOTO);
                    } else {
                        this.showMessage(content, 'No se ha podido guardar el evento!', this.MESSAGE_STAYHERE);
                        this.error = true;
                        this.loading = false;
                    }
                })
            .catch(e => this.handleError(e));
    }

    public eliminar(content) {
        this.showMessage(content, 'No implementada la eliminación', this.MESSAGE_STAYHERE);
    }

    public isNew() {
        return (this.selectedID.length === 0);
    }

    public volver() {
        this.router.navigate(['/eventos']);
    }

    isPendiente() {
        return +this.selectedEntity.dirty === 0;
    }

    isAprobado() {
        return +this.selectedEntity.dirty === 1;
    }

    setAprobado(dirty: number) {
        this.selectedEntity.dirty = dirty;
    }

    showMessage(content, message, action) {
        this.modalMessage = message;
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
            if (action === this.MESSAGE_GOTO) {
                this.router.navigate(['/eventos']);
            }
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    private handleError(error: any): Promise<any> {
        this.loading = false;
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
