import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '', component: LayoutComponent,
        children: [
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'empresa/:id', loadChildren: './empresa/empresa.module#EmpresaModule' },
            { path: 'empresas', loadChildren: './empresas/empresas.module#EmpresasModule' },
            { path: 'evento/:id', loadChildren: './evento/evento.module#EventoModule' },
            { path: 'eventos', loadChildren: './eventos/eventos.module#EventosModule' },
            { path: 'paquetes', loadChildren: './paquetes/paquetes.module#PaquetesModule' },
            { path: 'publicacion/:id', loadChildren: './publicacion/publicacion.module#PublicacionModule' },
            { path: 'publicaciones', loadChildren: './publicaciones/publicaciones.module#PublicacionesModule' },
            { path: 'publicidad/:id', loadChildren: './publicidad/publicidad.module#PublicidadModule' },
            { path: 'publicidades', loadChildren: './publicidades/publicidades.module#PublicidadesModule' },
            { path: 'sucursal/:id', loadChildren: './sucursal/sucursal.module#SucursalModule' },
            { path: 'sucursales', loadChildren: './sucursales/sucursales.module#SucursalesModule' },
            { path: 'categoria/:id', loadChildren: './categoria/categoria.module#CategoriaModule' },
            { path: 'categorias', loadChildren: './categorias/categorias.module#CategoriasModule' },
            { path: 'sponsor/:id', loadChildren: './sponsor/sponsor.module#SponsorModule' },
            { path: 'sponsors', loadChildren: './sponsors/sponsors.module#SponsorsModule' },

            // { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
            // { path: 'tables', loadChildren: './tables/tables.module#TablesModule' },
            // { path: 'forms', loadChildren: './form/form.module#FormModule' },
            // { path: 'bs-element', loadChildren: './bs-element/bs-element.module#BsElementModule' },
            // { path: 'grid', loadChildren: './grid/grid.module#GridModule' },
            // { path: 'components', loadChildren: './bs-component/bs-component.module#BsComponentModule' },
            // { path: 'blank-page', loadChildren: './blank-page/blank-page.module#BlankPageModule' },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule { }
