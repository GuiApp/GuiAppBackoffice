import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { PageHeaderModule } from './../../shared';
import { Ng2SmartTableModule } from 'ng2-smart-table';


import { EmpresasRoutingModule } from './empresas-routing.module';
import { EmpresasComponent } from './empresas.component';

@NgModule({
    imports: [
        FormsModule,
        Ng2SmartTableModule,
        CommonModule,
        EmpresasRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule
    ],
    declarations: [EmpresasComponent]
})
export class EmpresasModule { }
