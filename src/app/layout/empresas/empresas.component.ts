import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';


import { Empresa, EmpresaService } from '../../shared';

@Component({
    selector: 'app-tables',
    templateUrl: './empresas.component.html',
    styleUrls: ['./empresas.component.scss'],
    animations: [routerTransition()],
    providers: [ EmpresaService ]
})

export class EmpresasComponent implements OnInit {
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            // id: { title: 'Código', filter: false, },
            razonsocial: { title: 'Razón Social', filter: false, },
            cuit: { title: 'Cuit', filter: false, }
        },
        defaultStyle: false,
        rowClassFunction : function(row) {
                const aprobado = +row.data.dirty;
                if (aprobado === 1) {
                    return 'aprobado';
                } if (aprobado === 0) {
                    return 'noaprobado';
                } else {
                    return '';
                }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;


    public dataEstados: Array<any> = Array<any>();
    public selectedEntity: Empresa;
    public data: Array<any> = Array<any>();

    public constructor(public router: Router, private route: ActivatedRoute,
        private empresaService: EmpresaService) {
            this.source = new LocalDataSource(this.data);
    }

    public ngOnInit(): void {
        this.empresaService.getList().then(
            response => {
                if (response) {
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                } else {
                    alert('No se pudo obtener los datos de Empresas!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'id', search: query },
            { field: 'razonsocial', search: query },
            { field: 'cuit', search: query }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data;
        this.router.navigate(['/empresa', event.data.id]);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
