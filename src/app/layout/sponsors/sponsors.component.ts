import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';

import { Sponsor, SponsorService } from '../../shared';

@Component({
    selector: 'app-tables',
    templateUrl: './sponsors.component.html',
    styleUrls: ['./sponsors.component.scss'],
    animations: [routerTransition()],
    providers: [ SponsorService ]
})

export class SponsorsComponent implements OnInit {
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            id: { title: 'Código', filter: false, },
            razonsocial: { title: 'Razón Social', filter: false, }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;

    public dataEstados: Array<any> = Array<any>();
    public selectedEntity: Sponsor;
    public error = false;
    public loading = false;
    public data: Array<any> = Array<any>();

    public constructor(public router: Router, private route: ActivatedRoute,
        private sponsorService: SponsorService) {
            this.source = new LocalDataSource(this.data);
    }

    public ngOnInit(): void {
        this.sponsorService.getList().then(
            response => {
                if (response) {
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                } else {
                    alert('No se pudo obtener los datos de Empresas!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'id', search: query },
            { field: 'razonsocial', search: query }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data;
        this.router.navigate(['/sponsor', event.data.id]);
    }

    nuevo() {
        this.router.navigate(['/sponsor', -1]);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

}
