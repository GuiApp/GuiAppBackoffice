import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import {
    Empresa, EmpresaService
} from '../../shared/services';

@Component({
    selector: 'app-form',
    templateUrl: './empresa.component.html',
    styleUrls: ['./empresa.component.scss'],
    animations: [routerTransition()],
    providers: [
        EmpresaService]
})
export class EmpresaComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Empresa';

    public error = false;
    public loading = false;
    submitted = false;
    public selectedID: string;
    public selectedEntity: Empresa;
    public dataContactos: Array<any> = Array<any>();

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private location: Location,
        private modalService: NgbModal,
        private empresaService: EmpresaService) {
        this.selectedEntity = new Empresa();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.loading = true;
            this.selectedID = params['id'];
            this.getData();
            this.loading = false;
        });
    }

    public getData() {
      if (this.selectedID.length > 0) {
          this.empresaService.getByID(this.selectedID)
          .then(
              response => {
                  this.loading = false;
                  this.selectedEntity = response as Empresa;
                              })
          .catch(e => this.handleError(e));
      }
    }

    public volver() {
        this.router.navigate(['/empresas']);
    }

    public guardar(content) {
        this.empresaService
            .save(this.selectedEntity)
            .then(
                response => {
                    if (response) {
                        this.loading = false;
                        this.selectedID = response + '';
                        this.showMessage(content, 'La empresa se guardó correctamente', this.MESSAGE_GOTO);
                        // alert('Sponsor guardado correctamente');
                    } else {
                        this.showMessage(content, 'Ya existe un empresa con la razón social indicada!', this.MESSAGE_STAYHERE);
                        // alert('Ya existe un sponsor con la razón social indicada!');
                        this.error = true;
                        this.loading = false;
                    }
                })
            .catch(e => this.handleError(e));
    }

    public eliminar(content) {
        this.showMessage(content, 'No implementada la eliminación', this.MESSAGE_STAYHERE);
        // console.log('eliminar');
    }

    public isNew() {
        return (this.selectedID.length === 0);
    }

    isPendiente() {
        return +this.selectedEntity.dirty === 0;
    }

    isAprobado() {
        return +this.selectedEntity.dirty === 1;
    }

    setAprobado(dirty: number) {
        this.selectedEntity.dirty = dirty;
    }

    showMessage(content, message, action) {
        this.modalMessage = message;
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
            if (action === this.MESSAGE_GOTO) {
                this.router.navigate(['/empresas']);
            }
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    private handleError(error: any): Promise<any> {
        this.loading = false;
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
