import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';

import { Sucursal, SucursalService } from '../../shared';

@Component({
    selector: 'app-tables',
    templateUrl: './sucursales.component.html',
    styleUrls: ['./sucursales.component.scss'],
    animations: [routerTransition()],
    providers: [ SucursalService ]
})

export class SucursalesComponent implements OnInit {
settings = {
    noDataMessage: 'Datos no disponibles',
    pager: { display: true, perPage: 10 },
    columns: {
        // id: { title: 'Código', filter: false, },
        razonsocial: { title: 'Razón Social', filter: false, },
        direccion: { title: 'Dirección', filter: false, },
        telefono: { title: 'Teléfono', filter: false, },
        delivery: {
            title: 'Delivery', filter: false,
            type: 'html',
            valuePrepareFunction: (value) => {
                return (+value === 1) ? 'SI' : 'NO';
            }
        },
        veinticuatrohs: {
            title: '24 hs', filter: false,
            type: 'html',
            valuePrepareFunction: (value) => {
                return (+value === 1) ? 'SI' : 'NO';
            }
        },
        diashorarios: { title: 'Días y Horarios', filter: false, },
        palabrasclave: { title: 'Palabras Clave', filter: false, },
        latitud: { title: 'Latitud', filter: false, },
        longitud: { title: 'Longitud', filter: false, }
    },
    defaultStyle: false,
    rowClassFunction : function(row) {
            const aprobado = +row.data.dirty;
            if (aprobado === 1) {
                return 'aprobado';
            } if (aprobado === 0) {
                return 'noaprobado';
            } else {
                return '';
            }
    },
    actions: { add: false, edit: false, delete: false, },
    attr: {
        class: 'table dataTable table-striped table-bordered',
    }
};
source: LocalDataSource;

    public dataEstados: Array<any> = Array<any>();
    public selectedEntity: Sucursal;
    public error = false;
    public loading = false;
    public data: Array<any> = Array<any>();

    public constructor(public router: Router, private route: ActivatedRoute,
        private sucursalService: SucursalService) {
            this.source = new LocalDataSource(this.data);
    }

    public ngOnInit(): void {
        // var idempresa = '2';// ESTO ES TEMPORAL
        // this.sucursalService.getListByIdEmpresa(idempresa).then(
        this.sucursalService.getList().then(
            response => {
                if (response) {
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                } else {
                    alert('No se pudo obtener los datos de Empresas!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            // { field: 'id', search: query },
            { field: 'razonsocial', search: query },
            { field: 'direccion', search: query },
            { field: 'telefono', search: query },
            { field: 'delivery', search: query },
            { field: 'veinticuatrohs', search: query },
            { field: 'diashorarios', search: query },
            { field: 'dirty', search: query },
            { field: 'palabrasclave', search: query },
            { field: 'latitud', search: query },
            { field: 'Longitud', search: query }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data;
        this.router.navigate(['/sucursal', event.data.id]);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

}
