import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import {
    Categoria, CategoriaService
} from '../../shared/services';

@Component({
    selector: 'app-form',
    templateUrl: './categoria.component.html',
    styleUrls: ['./categoria.component.scss'],
    animations: [routerTransition()],
    providers: [
        CategoriaService]
})
export class CategoriaComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Categoria';

    public error = false;
    public loading = false;
    submitted = false;
    public selectedID: number;
    public selectedEntity: Categoria;
    public dataContactos: Array<any> = Array<any>();

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private location: Location,
        private modalService: NgbModal,
        private categoriaService: CategoriaService) {
        this.selectedEntity = new Categoria();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.loading = true;
            this.selectedID = +params['id'];
            this.getData();
            this.loading = false;
        });
    }

    public getData() {
      if (this.selectedID > -1) {
          this.categoriaService.getByID(this.selectedID)
          .then(
              response => {
                  this.loading = false;
                  this.selectedEntity = response as Categoria;
                              })
          .catch(e => this.handleError(e));
      }
    }


    public guardar(content) {
        this.categoriaService
            .save(this.selectedEntity)
            .then(
                response => {
                    if (response) {
                        this.loading = false;
                        this.selectedID = Number(response);
                        this.showMessage(content, 'El categoria se guardó correctamente', this.MESSAGE_GOTO);
                        // alert('Sponsor guardado correctamente');
                    } else {
                        this.showMessage(content, 'Ya existe un categoria con la razón social indicada!', this.MESSAGE_STAYHERE);
                        // alert('Ya existe un sponsor con la razón social indicada!');
                        this.error = true;
                        this.loading = false;
                    }
                })
            .catch(e => this.handleError(e));
    }

    public eliminar(content) {
        this.showMessage(content, 'No implementada la eliminación', this.MESSAGE_STAYHERE);
        // console.log('eliminar');
    }

    public isNew() {
        return (this.selectedID === -1);
    }
    public volver() {
        this.router.navigate(['/categorias']);
    }

    showMessage(content, message, action) {
        this.modalMessage = message;
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
            if (action === this.MESSAGE_GOTO) {
                this.router.navigate(['/categorias']);
            }
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    private handleError(error: any): Promise<any> {
        this.loading = false;
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
