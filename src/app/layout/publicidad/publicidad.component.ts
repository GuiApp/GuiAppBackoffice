import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import {
    Publicidad, PublicidadService,
    SponsorService, UploadFileService
} from '../../shared';
import { TWUtils } from 'app/shared/services/tw-utils';

@Component({
    selector: 'app-form',
    templateUrl: './publicidad.component.html',
    styleUrls: ['./publicidad.component.scss'],
    animations: [routerTransition()],
    providers: [
        SponsorService,
        PublicidadService,
        UploadFileService]
})

export class PublicidadComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Publicidad';

    public error = false;
    public loading = false;
    submitted = false;
    public selectedID: number;
    public selectedEntity: Publicidad;
    public dataContactos: Array<any> = Array<any>();
    public fecInicio: {};
    public fecFin: {};
    dataSponsors: Array<any> = Array<any>();

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private sponsorService: SponsorService,
        private modalService: NgbModal,
        private publicidadService: PublicidadService,
        private uploadService: UploadFileService) {
        this.selectedEntity = new Publicidad();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.loading = true;
            this.selectedID = +params['id'];
            this.getData();
            this.loading = false;
        });
    }

    public getData() {
        const p0 = this.sponsorService.getList();
        Promise.all([
            p0
        ]).then(([sponsors]) => {
            this.dataSponsors = sponsors;
            this.getPublicidad()
        });
    }

    public getPublicidad() {
        if (this.selectedID > -1) {
            this.publicidadService.getByID(this.selectedID)
                .then(
                    response => {
                        this.loading = false;
                        this.selectedEntity = response as Publicidad;
                        this.fecInicio = TWUtils.stringDateToJson(this.selectedEntity.fecinicio);
                        this.fecFin = TWUtils.stringDateToJson(this.selectedEntity.fecfin);
                    })
                .catch(e => this.handleError(e));
        }
    }

    fileChange(event) {
        const fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            const file: File = fileList[0];
            this.uploadService.upload(file).then(
                response => {
                    this.selectedEntity.imagen = response.generatedName;
                }).catch(e => this.handleError(e));
        }
    }

    public guardar(content) {
        this.selectedEntity.fecinicio = TWUtils.arrayDateToString(this.fecInicio);
        this.selectedEntity.fecfin = TWUtils.arrayDateToString(this.fecFin);
        this.publicidadService
            .save(this.selectedEntity)
            .then(
                response => {
                    if (response) {
                        this.loading = false;
                        this.selectedID = Number(response);
                        this.showMessage(content, 'El publicidad se guardó correctamente', this.MESSAGE_GOTO);
                    } else {
                        this.showMessage(content, 'Ya existe la publicidad indicada!', this.MESSAGE_STAYHERE);
                        this.error = true;
                        this.loading = false;
                    }
                })
            .catch(e => this.handleError(e));
    }

    public eliminar(content) {
        this.publicidadService.delete(this.selectedEntity).then(
            response => {
                const resp = Number(response);
                if ( resp > 0) {
                    this.showMessage(content, 'El registro se ha eliminado correctamente', this.MESSAGE_GOTO);
                } else if (resp === -1) {
                    this.showMessage(content, 'El registro no se ha eliminado porque se está utilizando.', this.MESSAGE_STAYHERE);
                } else {
                    this.showMessage(content, 'El registro no se ha eliminado por error de sistema.', this.MESSAGE_STAYHERE);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public isNew() {
        return (this.selectedID === -1);
    }

    public volver() {
        this.router.navigate(['/publicidades']);
    }

    showMessage(content, message, action) {
        this.modalMessage = message;
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
            if (action === this.MESSAGE_GOTO) {
                this.router.navigate(['/publicidades']);
            }
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    private handleError(error: any): Promise<any> {
        this.loading = false;
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
