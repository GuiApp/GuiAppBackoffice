import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { PublicidadRoutingModule } from './publicidad-routing.module';
import { PublicidadComponent } from './publicidad.component';
import { PageHeaderModule } from './../../shared';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        PublicidadRoutingModule,
        PageHeaderModule,
        FormsModule,
        NgbModule.forRoot(),
        ReactiveFormsModule
    ],
    declarations: [PublicidadComponent]
})
export class PublicidadModule { }
