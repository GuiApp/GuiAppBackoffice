import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import {
    PaqueteService, Paquete
} from '../../shared/services';

@Component({
    selector: 'app-form',
    templateUrl: './paquete.component.html',
    styleUrls: ['./paquete.component.scss'],
    animations: [routerTransition()],
    providers: [
        PaqueteService]
})
export class PaqueteComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Paquete';

    public error = false;
    public loading = false;
    submitted = false;
    public selectedID: number;
    public selectedEntity: Paquete;
    public dataContactos: Array<any> = Array<any>();

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private modalService: NgbModal,
        private paqueteService: PaqueteService) {
        this.selectedEntity = new Paquete();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedID = +params['id'];
            this.onGetEntityData();
        });
    }

    public onGetEntityData(): any {
        if (this.selectedID) {
            this.paqueteService.getByID(this.selectedID).then(
                response => {
                    this.selectedEntity = response as Paquete;
                }
            ).catch(e => this.handleError(e));
        }
    }
    // public constructor(public router: Router,
    //     private route: ActivatedRoute,
    //     private location: Location,
    //     private modalService: NgbModal,
    //     private paqueteService: PaqueteService) {
    //     this.selectedEntity = new Paquete();
    // }

    // public ngOnInit(): void {
    //     this.route.params.subscribe(params => {
    //         this.loading = true;
    //         this.selectedID = params['id'];
    //         this.getData();
    //         this.loading = false;
    //     });
    // }

    // public getData() {
    //   if (this.selectedID.length > 0) {
    //       this.paqueteService.getByID(this.selectedID)
    //       .then(
    //           response => {
    //               this.loading = false;
    //               this.selectedEntity = response as Evento;
    //                           })
    //       .catch(e => this.handleError(e));
    //   }
    // }


    // public guardar(content) {
    //     this.paqueteService
    //         .save(this.selectedEntity)
    //         .then(
    //             response => {
    //                 if (response) {
    //                     this.loading = false;
    //                     this.selectedID = response + '';
    //                     this.showMessage(content, 'El evento se guardó correctamente', this.MESSAGE_GOTO);
    //                 } else {
    //                     this.showMessage(content, 'No se ha podido guardar el evento!', this.MESSAGE_STAYHERE);
    //                     this.error = true;
    //                     this.loading = false;
    //                 }
    //             })
    //         .catch(e => this.handleError(e));
    // }

    // public eliminar(content) {
    //     this.showMessage(content, 'No implementada la eliminación', this.MESSAGE_STAYHERE);
    // }

    // public isNew() {
    //     return (this.selectedID.length === 0);
    // }

    // isPendiente() {
    //     return +this.selectedEntity.dirty === 0;
    // }

    // isAprobado() {
    //     return +this.selectedEntity.dirty === 1;
    // }

    // setAprobado(dirty: number) {
    //     this.selectedEntity.dirty = dirty;
    // }

    showMessage(content, message, action) {
        this.modalMessage = message;
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
            if (action === this.MESSAGE_GOTO) {
                this.router.navigate(['/eventos']);
            }
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    private handleError(error: any): Promise<any> {
        this.loading = false;
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
