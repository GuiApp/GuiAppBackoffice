import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import {
    Sponsor, SponsorService
} from '../../shared/services';

@Component({
    selector: 'app-form',
    templateUrl: './sponsor.component.html',
    styleUrls: ['./sponsor.component.scss'],
    animations: [routerTransition()],
    providers: [
        SponsorService]
})
export class SponsorComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Sponsor';

    public error = false;
    public loading = false;
    submitted = false;
    public selectedID: number;
    public selectedEntity: Sponsor;
    public dataContactos: Array<any> = Array<any>();

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private modalService: NgbModal,
        private sponsorService: SponsorService) {
        this.selectedEntity = new Sponsor();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.loading = true;
            this.selectedID = +params['id'];
            this.getData();
            this.loading = false;
        });
    }

    public getData() {
      if (this.selectedID > -1) {
          this.sponsorService.getByID(this.selectedID)
          .then(
              response => {
                  this.loading = false;
                  this.selectedEntity = response as Sponsor;
                              })
          .catch(e => this.handleError(e));
      }
    }

    public guardar(content) {
        this.sponsorService
            .save(this.selectedEntity)
            .then(
                response => {
                    if (response) {
                        this.loading = false;
                        this.selectedID = Number(response);
                        this.showMessage(content, 'El sponsor se guardó correctamente', this.MESSAGE_GOTO);
                        // alert('Sponsor guardado correctamente');
                    } else {
                        this.showMessage(content, 'Ya existe un Sponsor con la razón social indicada!', this.MESSAGE_STAYHERE);
                        // alert('Ya existe un sponsor con la razón social indicada!');
                        this.error = true;
                        this.loading = false;
                    }
                })
            .catch(e => this.handleError(e));
    }

    public eliminar(content) {
        this.sponsorService.delete(this.selectedEntity).then(
            response => {
                const resp = Number(response);
                if ( resp > 0) {
                    this.showMessage(content, 'El registro se ha eliminado correctamente', this.MESSAGE_GOTO);
                } else if (resp === -1) {
                    this.showMessage(content, 'El registro no se ha eliminado porque se está utilizando.', this.MESSAGE_STAYHERE);
                } else {
                    this.showMessage(content, 'El registro no se ha eliminado por error de sistema.', this.MESSAGE_STAYHERE);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public isNew() {
        return (this.selectedID === -1);
    }

    public volver() {
        this.router.navigate(['/sponsors']);
    }

    showMessage(content, message, action) {
        this.modalMessage = message;
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
            if (action === this.MESSAGE_GOTO) {
                this.router.navigate(['/sponsors']);
            }
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    private handleError(error: any): Promise<any> {
        this.loading = false;
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
