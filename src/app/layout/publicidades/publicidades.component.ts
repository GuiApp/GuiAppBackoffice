import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { DatePipe } from '@angular/common';

import { Publicidad, PublicidadService } from '../../shared';

@Component({
    selector: 'app-tables',
    templateUrl: './publicidades.component.html',
    styleUrls: ['./publicidades.component.scss'],
    animations: [routerTransition()],
    providers: [
        DatePipe,
        PublicidadService ]
})

export class PublicidadesComponent implements OnInit {
settings = {
    noDataMessage: 'Datos no disponibles',
    pager: { display: true, perPage: 10 },
    columns: {
        id: { title: 'Código', filter: false, },
        razonsocial: { title: 'Razón Social', filter: false, },
        fecinicio: {
            title: 'Fecha Inicio', filter: false,
            type: 'html',
            valuePrepareFunction: (value) => {
                return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy') + '</div>';
            }
        },
        fecfin: {
            title: 'Fecha Fin', filter: false,
            type: 'html',
            valuePrepareFunction: (value) => {
                return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy') + '</div>';
            }
        },
        imagen: { title: 'Imagen', filter: false, },
        url: { title: 'Url', filter: false, }
    },
    actions: { add: false, edit: false, delete: false, },
    attr: {
        class: 'table dataTable table-striped table-bordered',
    }
};
source: LocalDataSource;

    public dataEstados: Array<any> = Array<any>();
    public selectedEntity: Publicidad;
    public error = false;
    public loading = false;
    public data: Array<any> = Array<any>();

    public constructor(
        public router: Router,
        private datePipe: DatePipe,
        private publicidadService: PublicidadService) {
            this.source = new LocalDataSource(this.data);
    }

    public ngOnInit(): void {
        this.publicidadService.getList().then(
            response => {
                if (response) {
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                } else {
                    alert('No se pudo obtener los datos de Empresas!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'id', search: query },
            { field: 'razonsocial', search: query },
            { field: 'fecinicio', search: query },
            { field: 'fecfin', search: query },
            { field: 'imagen', search: query },
            { field: 'url', search: query }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data;
        this.router.navigate(['/publicidad', event.data.id]);
    }

    nuevo() {
        this.router.navigate(['/publicidad', -1]);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

}
