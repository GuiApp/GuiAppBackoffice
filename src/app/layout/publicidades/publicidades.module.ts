import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { PageHeaderModule } from './../../shared';
import { Ng2SmartTableModule } from 'ng2-smart-table';


import { PublicidadesRoutingModule } from './publicidades-routing.module';
import { PublicidadesComponent } from './publicidades.component';


@NgModule({
    imports: [
        FormsModule,
        Ng2SmartTableModule,
        CommonModule,
        PublicidadesRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule
    ],
    declarations: [PublicidadesComponent]
})
export class PublicidadesModule { }
