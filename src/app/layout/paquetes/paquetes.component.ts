import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';

import { Paquete, PaqueteService } from '../../shared';

@Component({
    selector: 'app-tables',
    templateUrl: './paquetes.component.html',
    styleUrls: ['./paquetes.component.scss'],
    animations: [routerTransition()],
    providers: [ PaqueteService ]
})

export class PaquetesComponent implements OnInit {
settings = {
    noDataMessage: 'Datos no disponibles',
    pager: { display: true, perPage: 10 },
    columns: {
        id: { title: 'Código', filter: false, },
        nombre: { title: 'Nombre', filter: false, },
        descripcion: { title: 'Descripción', filter: false, },
        costo: { title: 'Costo', filter: false, },
        duracion: { title: 'Duración', filter: false, }
    },
    actions: { add: false, edit: false, delete: false, },
    attr: {
        class: 'table dataTable table-striped table-bordered',
    }
};
source: LocalDataSource;

    public selectedEntity: Paquete;
    public data: Array<any> = Array<any>();

    public constructor(public router: Router, private route: ActivatedRoute,
        private paqueteService: PaqueteService) {
            this.source = new LocalDataSource(this.data);
    }

    public ngOnInit(): void {
        this.paqueteService.getList().then(
            response => {
                if (response) {
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                } else {
                    alert('No se pudo obtener los datos de Empresas!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'id', search: query },
            { field: 'nombre', search: query },
            { field: 'descripcion', search: query },
            { field: 'costo', search: query },
            { field: 'duracion', search: query }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data;
        // this.router.navigate(['/paquete', event.data.id]);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
