import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { PageHeaderModule } from './../../shared';
import { Ng2SmartTableModule } from 'ng2-smart-table';


import { PaquetesRoutingModule } from './paquetes-routing.module';
import { PaquetesComponent } from './paquetes.component';


@NgModule({
    imports: [
        FormsModule,
        Ng2SmartTableModule,
        CommonModule,
        PaquetesRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule
    ],
    declarations: [PaquetesComponent]
})
export class PaquetesModule { }
