export const environment = {
  production: true,
  apiUrl: 'https://guiappworldwide.com/admin/' + 'api/',
  imageStock: 'https://guiappworldwide.com/' + 'imgstock/',
  stockImgApi: 'https://guiappworldwide.com/' + 'imgstock/index.php'
  // imageStock: 'https://guiappworldwide.com/admin/' + 'imgstock/index.php',
};
